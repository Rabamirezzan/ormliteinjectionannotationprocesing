//package com.ormliteinjection.annotationprocessing;
//
//import javax.annotation.processing.Messager;
//import javax.annotation.processing.ProcessingEnvironment;
//import javax.tools.FileObject;
//import javax.tools.StandardLocation;
//import java.io.IOException;
//import java.io.Writer;
//
///**
// * Created by rramirezb on 29/12/2014.
// */
//public class Logger {
//    private static Writer writer;
//
//    public static void init(ProcessingEnvironment processingEnvironment){
//        try {
//            if(writer==null) {
//                FileObject log = processingEnvironment.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, "", "construction.log");
//                writer = log.openWriter();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void log(String format, Object... args){
//        try {
//            writer.append(String.format("".concat(format).concat("\n"), args));
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        }
//    }
//
//    public static void close(){
//        try {
//            if(writer!=null) {
//                writer.close();
//                writer = null;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//}
