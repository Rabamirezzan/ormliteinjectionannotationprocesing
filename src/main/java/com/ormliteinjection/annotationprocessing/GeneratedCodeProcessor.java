package com.ormliteinjection.annotationprocessing;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * Created by rramirezb on 29/12/2014.
 */

@SupportedAnnotationTypes("com.opesystems.tools.ormmodel.db.injection.GeneratedCode")

public class GeneratedCodeProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        return !roundEnv.processingOver();
    }
}
