package com.ormliteinjection.annotationprocessing;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 * Created by rramirezb on 18/12/2014.
 */
public class MessagePrinter {
    protected static void print(Diagnostic.Kind kind, ProcessingEnvironment processingEnv, Element element, String format, Object ... args) {
        try {
            if(element!=null){
                processingEnv.getMessager().printMessage(kind, String.format(format, args ),element);
            }else{
                processingEnv.getMessager().printMessage(kind, String.format(format, args));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printError(ProcessingEnvironment processingEnv, Element element, String format, Object ... args) {
        print(Diagnostic.Kind.ERROR, processingEnv, element, format, args);
    }

    public static void printInfo(ProcessingEnvironment processingEnv, Element element, String format, Object ... args) {
        print(Diagnostic.Kind.NOTE, processingEnv, element, format, args);
    }

    public static void printWarning(ProcessingEnvironment processingEnv, Element element, String format, Object ... args) {
        print(Diagnostic.Kind.WARNING, processingEnv, element, format, args);
    }
}
