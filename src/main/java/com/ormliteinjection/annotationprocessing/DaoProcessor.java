package com.ormliteinjection.annotationprocessing;


import com.codeinjection.tools.ElementTool;
import com.codeinjection.tools.FileGenerator;
import com.codeinjection.tools.log.FileStream;
import com.codeinjection.tools.log.Log;
import com.ormliteinjection.annotationprocessing.injection.*;
import com.ormliteinjection.annotationprocessing.injection.data.ModelData;
import com.ormliteinjection.core.db.injection.DaoInjection;
import com.ormliteinjection.core.db.injection.DaoInjectionInfo;
import com.ormliteinjection.core.db.injection.GeneratedCode;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.lang.annotation.Annotation;
import java.util.*;

/**
 * Created by rramirezb on 18/12/2014.
 */
//@SupportedAnnotationTypes(
//        {
//                "com.opesystems.ormliteinjection.core.db.injection.DaoInjection",
//                "com.opesystems.ormliteinjection.core.db.injection..DaoInjectionInfo"
//        })
public class DaoProcessor extends AbstractProcessor {
    public static ProcessingEnvironment PRO_ENV;
    ModelData modelData = null;
    FileGenerator fileGenerator;
    {
        fileGenerator = new FileGenerator();
    }
    public DaoProcessor() {

    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(DaoInjection.class.getName(), DaoInjectionInfo.class.getName()));
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        FileStream stream = new FileStream("Log.log");
        stream.setProcessingEnv(processingEnv);
        Log.getInstance().setMessager(stream);
    }
    //public static File sourceOutput;

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        PRO_ENV = processingEnv;

        //sourceOutput = PathVisor.getSourcePath(processingEnv.getFiler());
//        MessagePrinter.printInfo(processingEnv, null, "El path es %s", path);
//        boolean doProcess = path!=null;
//        if(doProcess) {

        try {

            Log.i("INIT", "NEW ROUND ===============================================");


            Log.i("INIT", "pasando por process round %s, still processing %s", roundEnv.getRootElements(), !roundEnv.processingOver());
            if (roundEnv.processingOver()) {

                Tool.printInfo("se abucó");
                Log.close();
                return true;
            }


            try {
                Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(DaoInjectionInfo.class);

                boolean hasDaoInfo = false;


                Tool.printInfo("Elements with %s, %s. Thread %s", DaoInjectionInfo.class, elements, Thread.currentThread().getId());
                if (hasDaoInfo = !elements.isEmpty()) {
                    //            try {
                    //                FileObject writer = processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, "ormliteinjection.config", "hola.config");
                    //                writer.openWriter().close();
                    //            } catch (Exception e) {
                    //                Tool.printError(e);
                    //                e.printStackTrace();
                    //            }


                    Tool.printInfo("leyendo modelData de elements");
                    if (modelData == null)
                        modelData = OrmHelperGenerator.generate(processingEnv, elements, modelData, fileGenerator);
                    // Tool.saveModelData(modelData, processingEnv);
                } else {
                    Tool.printInfo("tratando de leer información de .config %s", modelData);
                    // modelData = Tool.readModelData(processingEnv);
                    hasDaoInfo = modelData != null;
                }
                elements = roundEnv.getElementsAnnotatedWith(DaoInjection.class);
                if (!elements.isEmpty()) {

                    Tool.printInfo("Elements with %s, %s. Thread %s", DaoInjection.class, elements, Thread.currentThread().getId());
                    if (hasDaoInfo) {
                        ClassesToStoreGenerator.generate(processingEnv, elements, modelData, fileGenerator);
                        DbGenerator.generate(processingEnv, elements, modelData, fileGenerator);

                        Tool.printInfo("before create daos, package dao is %s", modelData.getDaoPackage());
                        for (Element e : elements) {

                            Element element;
                            try {

                                if (e instanceof VariableElement) {
                                    TypeMirror typeMirror = ((VariableElement) e).asType();
                                    element = (ElementTool.asTypeElement(typeMirror));
                                } else {
                                    element = e;
                                }
                                modelData = StatementGenerator.generate(processingEnv, element, modelData, fileGenerator);
                                DaoGenerator.generate(processingEnv, element, modelData, fileGenerator);
                            } catch (Exception e1) {
                                Tool.printWarning(e1);
                                e1.printStackTrace();
                            }
                        }
                    } else {
                        for (Element e : elements) {
                            MessagePrinter.printWarning(processingEnv, e, "Check if %1$sDao and %1$sWhere classes of this class %1$s are created.\n" +
                                    " Check if %1$sWhere class is complete (all annotated fields with DatabaseField are created as methods).\n" +
                                    "If not, rebuild project.", e.getSimpleName());
                        }
                    }
                }
            } catch (Throwable e) {
                //Logger.log("big exc %s", Tool.getStacktrace(e));
                Log.i("INIT", "%s", Tool.getStacktrace(e));
                //  Log.close();
            }

            if (!roundEnv.processingOver()) {
                Class<GeneratedCode> cls = GeneratedCode.class;
                List<Class<? extends Annotation>> anns = new ArrayList<Class<? extends Annotation>>();
                anns.add(GeneratedCode.class);
                fileGenerator.generateFiles(processingEnv, anns);
            }
        } catch (Exception e) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, Tool.getStacktrace(e));
            e.printStackTrace();
        }
//        }
        return true;
    }


    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
