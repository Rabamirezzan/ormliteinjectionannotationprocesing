package com.ormliteinjection.annotationprocessing;

import javax.annotation.processing.Filer;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import java.io.File;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by rramirezb on 18/12/2014.
 */
public class PathVisor {
    public static File getSourcePath(Filer filer) {

        File path = null;
        if (filer != null) {
            FileObject source = null;
            try {
                source = filer.getResource(StandardLocation.SOURCE_OUTPUT, "", "__VisorStub");
                path = new File(source.toUri());
                path = path.getParentFile();
                Writer writer = source.openWriter();
                writer.append("public class __VisorStub{}");
                writer.flush();
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return path;
    }
}
