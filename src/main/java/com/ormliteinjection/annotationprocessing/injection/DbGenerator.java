package com.ormliteinjection.annotationprocessing.injection;

import android.content.Context;
import com.codeinjection.tools.ElementTool;
import com.codeinjection.tools.FileGenerator;
import com.ormliteinjection.annotationprocessing.injection.data.ModelData;
import com.ormliteinjection.core.db.DBInstance;
import com.ormliteinjection.core.db.helper.AndroidDatabaseInfo;
import com.ormliteinjection.core.db.helper.JavaDatabaseInfo;
import com.ormliteinjection.core.db.helper.XDBHelper;
import com.sun.codemodel.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rramirezb on 26/12/2014.
 */
public class DbGenerator {

    public static final String INFO_NAME = "info";
    public static final String ANDROID_INFO_NAME = "androidInfo";
    private static final java.lang.String CONTEXT_NAME = "context";
    private static final java.lang.String PLATFORM_NAME = "platform";

    public static void generate(ProcessingEnvironment processingEnv, Set<? extends Element> elements, ModelData modelData, FileGenerator fileGenerator) {
        List<TypeElement> typeElements = new ArrayList<TypeElement>();
        for (Element e : elements) {
            if (e instanceof TypeElement) {
                typeElements.add((TypeElement) e);
            }else if(e instanceof VariableElement){
                TypeMirror tm = e.asType();
                typeElements.add(ElementTool.asTypeElement(tm));
            }
        }

        if (!typeElements.isEmpty()) {
            generate(processingEnv, typeElements, modelData, fileGenerator);
        }
    }

    private static void generate(ProcessingEnvironment processingEnv, List<TypeElement> typeElements, ModelData modelData, FileGenerator fileGenerator) {
        if (!typeElements.isEmpty()) {
            String daosClassName = modelData.getDbClass();

            JCodeModel codeModel = fileGenerator.getCodeModel(daosClassName);
            try {
                AtomicBoolean alreadyDefined = new AtomicBoolean();
                JDefinedClass dbClass = Tool.getPublicFinalClass(fileGenerator, daosClassName, true, alreadyDefined);


                if (!alreadyDefined.get()) {
                    // JFieldVar platform = generatePlatformField(dbClass);

                    if (Tool.isJavaPlatform(modelData.getPlatform()) ||
                            Tool.isBothPlatforms(modelData.getPlatform())) {
                        JFieldVar infoField = generateInfoField(dbClass);
                        generateJavaInfoDBSingleton(codeModel, dbClass, "get");
                        generateDefaultConstructor(modelData, codeModel, dbClass, infoField);
                    }
                    if (Tool.isAndroidPlatform(modelData.getPlatform()) ||
                            Tool.isBothPlatforms(modelData.getPlatform())) {
                        JFieldVar context = generateContextField(dbClass);
                        JFieldVar androidInfoField = generateAndroidInfoField(dbClass);
                        generateAndroidInfoDBSingleton(codeModel, dbClass, "get");
                        generateAndroidDefaultConstructor(modelData, codeModel, dbClass, context, androidInfoField);
                    }
                }
                //JFieldVar platform = getPlatformField(dbClass);
                generateDaoCalls(codeModel, dbClass, typeElements, modelData);


            } catch (JClassAlreadyExistsException e) {
                e.printStackTrace();
            }
        }
    }

    private static void generateDaoCalls(JCodeModel codeModel, JDefinedClass cls, List<TypeElement> typeElements, ModelData modelData) {
        for (TypeElement e : typeElements) {
            generateDaoCall(codeModel, cls, e, modelData);
        }


    }


    private static void generateDaoCall(JCodeModel codeModel, JDefinedClass cls, TypeElement element, ModelData modelData) {
        generateDaoFieldCall(codeModel, cls, element, modelData);
        generateDaoMethodCall(codeModel, cls, element, modelData);

    }

    private static void generateDaoFieldCall(JCodeModel codeModel, JDefinedClass cls, TypeElement element, ModelData modelData) {
        String className = Tool.getDaoName(modelData, element, DaoGenerator.DAO_SUFFIX);
        JClass ref = codeModel.ref(className);
        String daoName = Tool.toLowerFirstLetter(ref.name());
        JFieldVar field = cls.field(JMod.PRIVATE, ref, daoName);

    }

    private static void generateDaoMethodCall(JCodeModel codeModel, JDefinedClass cls, TypeElement element, ModelData modelData) {
        String className = Tool.getDaoName(modelData, element, DaoGenerator.DAO_SUFFIX);
        JClass ref = codeModel.ref(className);
        String daoName = Tool.toLowerFirstLetter(ref.name());
        JMethod method = cls.method(JMod.PUBLIC, ref, "get" + Tool.toUpperFirstLetter(daoName));

        JVar platform = null;
        if (Tool.isBothPlatforms(modelData.getPlatform())) {
            platform = method.param(XDBHelper.Platform.class, "platform");
        }
        JBlock body = method.body();

        JClass platformRef = codeModel.ref(XDBHelper.Platform.class);
        if (Tool.isJavaPlatform(modelData.getPlatform())) {
            body._if(JExpr.ref(daoName).eq(JExpr._null()))._then().
                    assign(JExpr.refthis(daoName), ref.staticInvoke("get").arg(JExpr.refthis(INFO_NAME)));
            body._return(JExpr.refthis(daoName));
        } else if (Tool.isAndroidPlatform(modelData.getPlatform())) {
            body._if(JExpr.ref(daoName).eq(JExpr._null()))._then().
                    assign(JExpr.refthis(daoName), ref.staticInvoke("get").arg(JExpr.refthis(CONTEXT_NAME)).arg(JExpr.refthis(ANDROID_INFO_NAME)));
            body._return(JExpr.refthis(daoName));

        } else if (Tool.isBothPlatforms(modelData.getPlatform())) {
            JConditional conditional = body._if(JExpr.ref(daoName).eq(JExpr._null()))._then()._if(platformRef.staticRef("JAVA").invoke("equals").arg(platform));
            conditional._then().assign(JExpr.refthis(daoName), ref.staticInvoke("get").arg(JExpr.refthis(INFO_NAME)));
            conditional._else().assign(JExpr.refthis(daoName), ref.staticInvoke("get").arg(JExpr.refthis(CONTEXT_NAME)).arg(JExpr.refthis(ANDROID_INFO_NAME)));
            body._return(JExpr.refthis(daoName));
        }

//        JConditional conditional = body._if(platformRef.staticRef("JAVA").invoke("equals").arg(platform));
//        conditional._then()._return(ref.staticInvoke("get").arg(JExpr.refthis(INFO_NAME)));
//        conditional._else()._return(ref.staticInvoke("get").arg(JExpr.refthis(CONTEXT_NAME)).arg(JExpr.refthis(ANDROID_INFO_NAME)));

    }

    public static JFieldVar generatePlatformField(JDefinedClass cls) {
        return cls.field(JMod.PRIVATE, XDBHelper.Platform.class, PLATFORM_NAME);
    }

    public static JFieldVar getPlatformField(JDefinedClass cls) {
        return cls.fields().get(PLATFORM_NAME);
    }

    public static JFieldVar generateContextField(JDefinedClass cls) {
        return cls.field(JMod.PRIVATE, Context.class, CONTEXT_NAME);
    }

    public static JFieldVar generateAndroidInfoField(JDefinedClass cls) {
        return cls.field(JMod.PRIVATE, AndroidDatabaseInfo.class, ANDROID_INFO_NAME);
    }

    public static JFieldVar generateInfoField(JDefinedClass cls) {
        return cls.field(JMod.PRIVATE, JavaDatabaseInfo.class, INFO_NAME);
    }

    private static void callDBInstanceByJavaInfo(JMethod constructor, JFieldVar dbInstance, JVar dbJavaInfo, ModelData modelData, JCodeModel codeModel) {
        JClass ormHelperRef = codeModel.ref(modelData.getOrmHelperClass());
        JBlock body = constructor.body();
        JInvocation dbInstanceCall = JExpr._new(ormHelperRef).arg(dbJavaInfo).invoke("getInstance");
        body.assign(dbInstance, dbInstanceCall);
    }

    private static void callDBInstanceByAndroidInfo(JMethod constructor, JFieldVar dbInstance, JVar context, JVar dbAndroidInfo, ModelData modelData, JCodeModel codeModel) {
        JClass ormHelperRef = codeModel.ref(modelData.getOrmHelperClass());
        JBlock body = constructor.body();
        JInvocation dbInstanceCall = JExpr._new(ormHelperRef).arg(context).arg(dbAndroidInfo).invoke("getInstance");
        body.assign(dbInstance, dbInstanceCall);
    }

    private static void generateGetDBMethod(JFieldVar dbInstance, JDefinedClass cls){
        JMethod getDb = cls.method(JMod.PUBLIC, DBInstance.class, "getDb");
        getDb.body()._return(dbInstance);
    }

    public static void generateDefaultConstructor(ModelData modelData, JCodeModel codeModel, JDefinedClass cls, JFieldVar info) {

        JFieldVar dbInstance = cls.field(JMod.PRIVATE | JMod.FINAL, DBInstance.class, "db");
        JMethod constructor = cls.constructor(JMod.PRIVATE);
        JVar dbParam = constructor.param(JavaDatabaseInfo.class, INFO_NAME);
        JBlock body = constructor.body();
        body.assign(JExpr.refthis(info.name()), JExpr.direct(INFO_NAME));
        //new OrmHelper(context, dbInfo).getInstance();
        callDBInstanceByJavaInfo(constructor, dbInstance, dbParam, modelData, codeModel);
        generateGetDBMethod(dbInstance,cls);
//        body.assign(JExpr.refthis(CONTEXT_NAME), JExpr._null());
//        body.assign(JExpr.refthis(ANDROID_INFO_NAME), JExpr._null());
//        body.assign(JExpr.refthis(PLATFORM_NAME),
//                codeModel.ref(XDBHelper.Platform.class).staticRef(XDBHelper.Platform.JAVA.name()));
    }

    public static void generateAndroidDefaultConstructor(ModelData modelData, JCodeModel codeModel, JDefinedClass cls, JFieldVar context, JFieldVar info) {
        JFieldVar dbInstance = cls.field(JMod.PRIVATE | JMod.FINAL, DBInstance.class, "db");
        JMethod constructor = cls.constructor(JMod.PRIVATE);
        JVar contextParam = constructor.param(Context.class, CONTEXT_NAME);
        JVar androidInfoParam = constructor.param(AndroidDatabaseInfo.class, INFO_NAME);
        JBlock body = constructor.body();
        body.assign(JExpr.refthis(context.name()), JExpr.direct(CONTEXT_NAME));
        body.assign(JExpr.refthis(info.name()), JExpr.direct(INFO_NAME));

        callDBInstanceByAndroidInfo(constructor,dbInstance,contextParam, androidInfoParam,modelData,codeModel);
        generateGetDBMethod(dbInstance,cls);
//        body.assign(JExpr.refthis(INFO_NAME), JExpr._null());
//        body.assign(JExpr.refthis(PLATFORM_NAME),
//                codeModel.ref(XDBHelper.Platform.class).staticRef(XDBHelper.Platform.ANDROID.name()));
    }

    public static void generateJavaInfoDBSingleton(JCodeModel codeModel, JDefinedClass cls, String instanceMethodName) {
        JMethod singletonMethod = cls.method(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, cls, instanceMethodName);
        singletonMethod.param(JavaDatabaseInfo.class, INFO_NAME);
        singletonMethod.body()._return(JExpr._new(cls).arg(JExpr.direct(INFO_NAME)));


    }

    public static void generateAndroidInfoDBSingleton(JCodeModel codeModel, JDefinedClass cls, String instanceMethodName) {
        JMethod singletonMethod = cls.method(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, cls, instanceMethodName);
        singletonMethod.param(Context.class, CONTEXT_NAME);
        singletonMethod.param(AndroidDatabaseInfo.class, INFO_NAME);
        singletonMethod.body()._return(JExpr._new(cls).arg(JExpr.direct(CONTEXT_NAME)).arg(JExpr.direct(INFO_NAME)));


    }
}
