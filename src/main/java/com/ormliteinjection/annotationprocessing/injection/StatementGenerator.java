package com.ormliteinjection.annotationprocessing.injection;

import com.codeinjection.tools.FileGenerator;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.StatementBuilder;
import com.ormliteinjection.annotationprocessing.MessagePrinter;
import com.ormliteinjection.annotationprocessing.injection.data.ModelData;
import com.ormliteinjection.core.db.xtend.XStatement;
import com.sun.codemodel.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;



/**
 * Created by rramirezb on 18/12/2014.
 */
public class StatementGenerator {
    public static final String STATEMENT_SUFFIX = "Statement";
    public static ModelData generate(ProcessingEnvironment processingEnv, Element element, ModelData modelData, FileGenerator fileGenerator) {

        ModelData result = new ModelData();

        if (element instanceof TypeElement && element.getKind().isClass()) {
            result = generate(processingEnv, (TypeElement) element, modelData,fileGenerator);
        }
        return result;
    }


    public static ModelData generate(ProcessingEnvironment processingEnv, TypeElement element, ModelData modelData,FileGenerator fileGenerator) {



        ModelData _modelData = null;



            _modelData=  generate(element,modelData,fileGenerator);
        if(_modelData==null){
            MessagePrinter.printWarning(processingEnv, element, "Class %s will not have dao. Has not id field.", element.getQualifiedName());
        }
        return _modelData;
    }

    public static ModelData generate(TypeElement element, ModelData modelData,FileGenerator fileGenerator) {



        ModelData _modelData = Tool.getValidModelData(modelData);
        _modelData = Tool.addModelClassData( element, _modelData);
        Tool.addWhereClassData(element, _modelData);
        if(!_modelData.getIdTypeClass().equals("")) {

            String whereClassName = _modelData.getWhereClass();

            JCodeModel codeModel = fileGenerator.getCodeModel(whereClassName);
            JClass typeRef = codeModel.ref(_modelData.getModelClass());
            JClass idTypeRef = codeModel.ref(_modelData.getIdTypeClass());
            JClass whereRef = codeModel.directClass(whereClassName);


            JClass xWhereClass = codeModel.ref(XStatement.class).narrow(
                    typeRef,
                    idTypeRef,
                    whereRef);
            try {

                AtomicBoolean alreadyDefined = new AtomicBoolean();
                JDefinedClass elementWhereClass = Tool.getPublicFinalClass(fileGenerator, whereClassName, false, alreadyDefined);
                elementWhereClass._extends(xWhereClass);


                JClass statementBuilderRef = codeModel.ref(StatementBuilder.class).narrow(typeRef, idTypeRef);
                JMethod defaultConstructor = elementWhereClass.constructor(JMod.PUBLIC);
                defaultConstructor.param(statementBuilderRef, "builder");
                defaultConstructor.body().directStatement("super(builder);");

                generateMethodFields(element, elementWhereClass, codeModel);
                //MessagePrinter.printInfo(DaoProcessor.PRO_ENV, null, "se creará %s ", whereClassName);

                System.out.printf("se creará %s \n", whereClassName);

            } catch (Exception e) {
                System.out.printf("%s\n", e.getMessage());
                e.printStackTrace();
                //Tool.printError(e);
            }
        }
        return _modelData;
    }

    protected static String getForeignStatement(VariableElement field) {
        boolean foreign = Tool.isForeign(field);
        String result = "";
        if (foreign) {
            result = String.format("setForeignClass(%s.class);", field.asType());
        }

        return result;
    }

    protected static JBlock getForeignStatement(VariableElement field, JBlock body, JCodeModel codeModel) {
        boolean foreign = Tool.isForeign(field);
        String result = "";
        if (foreign) {
            body.invoke("setForeignClass").arg(JExpr.dotclass(codeModel.ref(field.asType().toString())));
            //result = String.format("setForeignClass(%s.class);", field.asType());
        }

        return body;
    }

    protected static String getBodyStatement(VariableElement field) {
        String foreignStatement = getForeignStatement(field);
        String name = Tool.getName(field);
        String sBody = "final String column = \"%s\";\n" +
                "setNameField(column);\n" +
                "%s\n" +
                "addOrderByColumn(column);\n" +
                "return this;";
        return String.format(sBody, name, foreignStatement);
    }

    protected static void getBodyStatement(VariableElement field, JBlock body, JCodeModel codeModel) {

        String name = Tool.getName(field);

        try {

            body.directStatement(String.format("final String column = \"%s\";", name));
            body.directStatement(String.format("setNameField(column);"));
            getForeignStatement(field, body, codeModel);
            body.directStatement(String.format("addColumnToCache(column);"));
            body.directStatement("return this;");
        } catch (Exception e) {
            Tool.printError(e);
            e.printStackTrace();
        }
        return;
    }


    protected static void generateMethodFields(TypeElement element, JDefinedClass definedClass, JCodeModel codeModel) {
        List<? extends VariableElement> fields = Tool.getFieldsWithAnnotation(element, DatabaseField.class);
        JMethod method;

        for (VariableElement field : fields) {
            try {
                method = definedClass.method(JMod.PUBLIC, definedClass, field.getSimpleName().toString());
                setJavaDoc(method, field);
                JBlock body = method.body();
                getBodyStatement(field, body, codeModel);
            } catch (Exception e) {
                Tool.printError(e);
                e.printStackTrace();
            }

        }
    }

    protected static void setJavaDoc(JMethod method, VariableElement field){
        method.javadoc().append(String.format("{@link %s} value.", Tool.getType(field).toString()));
    }
}
