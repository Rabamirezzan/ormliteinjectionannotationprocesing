package com.ormliteinjection.annotationprocessing.injection.data;


import com.ormliteinjection.core.db.helper.XDBHelper;

/**
 * Created by rramirezb on 19/12/2014.
 */
public class ModelData {
    private String dbClass;
    private String modelClass;
    private String idTypeClass;
    private String whereClass;
    private String ormHelperClass;
    private XDBHelper.Platform platform;

    {
        dbClass = "";
        modelClass = "";
        idTypeClass = "";
        whereClass = "";
        ormHelperClass = "";
    }

    private String daoPackage;
    private String classesToStore;

    public String getOrmHelperClass() {
        return ormHelperClass;
    }

    public void setOrmHelperClass(String ormHelperClass) {
        this.ormHelperClass = ormHelperClass;
    }

    public String getModelClass() {
        return modelClass;
    }

    public void setModelClass(String modelClass) {
        this.modelClass = modelClass;
    }

    public String getIdTypeClass() {
        return idTypeClass;
    }

    public void setIdTypeClass(String idTypeClass) {
        this.idTypeClass = idTypeClass;
    }

    public String getWhereClass() {
        return whereClass;
    }

    public void setWhereClass(String whereClass) {
        this.whereClass = whereClass;
    }

    public void setDaoPackage(String daoPackage) {
        this.daoPackage = daoPackage;
    }

    public String getDaoPackage() {
        return daoPackage;
    }

    public void setClassesToStore(String classesToStore) {
        this.classesToStore = classesToStore;
    }

    public String getClassesToStore() {
        return classesToStore;
    }

    public String getDbClass() {
        return dbClass;
    }

    public void setDbClass(String dbClass) {
        this.dbClass = dbClass;
    }

    public XDBHelper.Platform getPlatform() {
        return platform;
    }

    public void setPlatform(XDBHelper.Platform platform) {
        this.platform = platform;
    }
}
