package com.ormliteinjection.annotationprocessing.injection;


import com.codeinjection.tools.FileGenerator;
import com.ormliteinjection.annotationprocessing.MessagePrinter;
import com.ormliteinjection.annotationprocessing.injection.data.ModelData;
import com.ormliteinjection.core.db.DBInstance;
import com.ormliteinjection.core.db.helper.AndroidDatabaseInfo;
import com.ormliteinjection.core.db.helper.JavaDatabaseInfo;
import com.ormliteinjection.core.db.helper.XDBHelper;
import com.sun.codemodel.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.naming.Context;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rramirezb on 20/12/2014.
 */
public class OrmHelperGenerator {
    public static final String DAO_PACKAGE_DEFAULT = "com.ormliteinjection.dao";
    public static final String ORM_HELPER_CLASS_DEFAULT = "DefaultOrmHelper";
    public static final String DATABASE_REF_CLASS_DEFAULT = "Daos";

//    public static void generate(ProcessingEnvironment processingEnv){
//                   generate(processingEnv);
//
//    }
    public static ModelData generate(ProcessingEnvironment processingEnv, Set<? extends Element> elements, ModelData modelData, FileGenerator fileGenerator){

        modelData = Tool.getValidModelData(modelData);
        Tool.addOrmHelperInfo(elements, modelData);
        Tool.printInfo("after addOrmHelperInfo %s", modelData.getDaoPackage());
        String ormHelperClassName = modelData.getOrmHelperClass();
        Tool.setPlatform(modelData, elements);

        JCodeModel codeModel = fileGenerator.getCodeModel(ormHelperClassName);
        try {
            AtomicBoolean alreadyDefined = new AtomicBoolean();
            JDefinedClass ormHelperClass = Tool.getPublicFinalClass(fileGenerator, ormHelperClassName,  false, alreadyDefined);
            if(!alreadyDefined.get()) {
                ormHelperClass._extends(XDBHelper.class);

                if (Tool.isJavaPlatform(modelData.getPlatform())||Tool.isBothPlatforms(modelData.getPlatform())) {
                    createJavaConstructor(ormHelperClass, codeModel);
                }
                if(Tool.isAndroidPlatform(modelData.getPlatform())||Tool.isBothPlatforms(modelData.getPlatform())){
                    try {
                        Class<Context> test = Context.class;
                        test.getName();
                        createAndroidConstructor(ormHelperClass, codeModel);
                    } catch (NoClassDefFoundError e) {

                        deleteAndroidConstructor(ormHelperClass);
                        e.printStackTrace();
                        MessagePrinter.printWarning(processingEnv, null, "Error on creating Android constructor");
                        Tool.printWarning(e);
                    }
                }
                createSingletonMethod(ormHelperClass, codeModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Tool.printWarning(e);
        }
        return modelData;
    }

    private static void deleteAndroidConstructor(JDefinedClass ormHelperClass){
        JMethod c = ormHelperClass.getConstructor(new JType[]{});
        Iterator<JMethod> cs = ormHelperClass.constructors();
        while(cs.hasNext()){
            JMethod ref = cs.next();
            if(ref.equals(c)){
                cs.remove();
            }
        }
    }

    private static void createSingletonMethod(JDefinedClass ormHelperClass, JCodeModel codeModel) {
        JMethod method = ormHelperClass.method(JMod.PUBLIC, DBInstance.class, "getInstance");
        method.body().
                _return(
                        JExpr.invoke("getInstance").
                                arg(JExpr.direct("\"unique_instance\"")).
                                arg(JExpr.dotclass(codeModel.ref(ClassesToStoreGenerator.CLASSES_TO_STORE_CLASS_NAME))));
    }

    public static JMethod createJavaConstructor(JDefinedClass ormHelperClass, JCodeModel codeModel){
        JMethod javaConstructor = ormHelperClass.constructor(JMod.PUBLIC);
        javaConstructor.param(codeModel.ref(JavaDatabaseInfo.class), "info");
        setJavaBody(javaConstructor);
        return javaConstructor;
    }
    public static void setJavaBody(JMethod constructor){
        constructor.body().directStatement("super(info);");
    }

    public static JMethod createAndroidConstructor(JDefinedClass ormHelperClass, JCodeModel codeModel){
        JMethod androidConstructor = ormHelperClass.constructor(JMod.PUBLIC);
        androidConstructor.param(codeModel.ref(android.content.Context.class), "context");
        androidConstructor.param(codeModel.ref(AndroidDatabaseInfo.class), "info");
        setAndroidBody(androidConstructor);

        return androidConstructor;
    }
    public static void setAndroidBody(JMethod constructor){
        constructor.body().directStatement("super(context, info);");
    }
}
