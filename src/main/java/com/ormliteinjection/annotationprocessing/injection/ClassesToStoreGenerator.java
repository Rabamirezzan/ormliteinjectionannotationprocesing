package com.ormliteinjection.annotationprocessing.injection;

import com.codeinjection.tools.ElementTool;
import com.codeinjection.tools.FileGenerator;
import com.ormliteinjection.annotationprocessing.injection.data.ModelData;
import com.ormliteinjection.core.db.ClassesToStore;
import com.sun.codemodel.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rramirezb on 19/12/2014.
 */
public class ClassesToStoreGenerator {

    public static final String CLASSES_TO_STORE_CLASS_NAME = "ClassesToStoreImpl";
    public static void generate(ProcessingEnvironment processingEnv, Set<? extends Element> elements, ModelData modelData, FileGenerator fileGenerator) {
        List<TypeElement> typeElements = new ArrayList<TypeElement>();
        for (Element e : elements) {
            if (e instanceof TypeElement) {
                typeElements.add((TypeElement) e);
            }else if(e instanceof VariableElement){
                TypeMirror typeMirror = ((VariableElement) e).asType();
                typeElements.add(ElementTool.asTypeElement(typeMirror));
            }
        }

        if (!typeElements.isEmpty()) {
            generate(processingEnv, typeElements, modelData, fileGenerator);
        }

    }

    public static void generate(ProcessingEnvironment processingEnv, List<? extends TypeElement> elements, ModelData modelData, FileGenerator fileGenerator) {

        modelData = Tool.addClassesToStoreInfo(modelData, CLASSES_TO_STORE_CLASS_NAME);
        String classesToStoreClassName = modelData.getClassesToStore();
        JCodeModel codeModel = fileGenerator.getCodeModel(classesToStoreClassName);
        try {
            AtomicBoolean alreadyDefined= new AtomicBoolean();

            JDefinedClass _class = Tool.getPublicFinalClass(fileGenerator, classesToStoreClassName, true, alreadyDefined);
            _class._implements(ClassesToStore.class);

            generateBody(codeModel, _class, elements);
        } catch (JClassAlreadyExistsException e) {
            e.printStackTrace();
        }
    }

    protected static void generateBody(JCodeModel codeModel, JDefinedClass _class, List<? extends TypeElement> elements) {
//        val field = storeClass.addField("model" + annotatedClass.simpleName + "Class") [
//                initializer = ''' «annotatedClass.simpleName».class'''
//        type = _returnType
//        static = true
//        final = true
//        visibility = Visibility.PUBLIC
//        ]
        for (TypeElement element : elements) {
            String fullName =element.getQualifiedName().toString();
            String simpleName = String.format("model%sClass", element.getSimpleName());
            if(!_class.fields().containsKey(simpleName)) {
                _class.field(JMod.PUBLIC | JMod.FINAL | JMod.STATIC,
                        Tool.getClassWithWildcardsString(codeModel, Class.class, fullName),
                        simpleName, JExpr.dotclass(codeModel.ref(fullName)));
            }
        }

    }

}
