package com.ormliteinjection.annotationprocessing.injection;

import android.content.Context;
import com.codeinjection.tools.FileGenerator;
import com.ormliteinjection.annotationprocessing.injection.data.ModelData;
import com.ormliteinjection.core.db.DBInstance;
import com.ormliteinjection.core.db.helper.AndroidDatabaseInfo;
import com.ormliteinjection.core.db.helper.JavaDatabaseInfo;
import com.ormliteinjection.core.db.xtend.XDao;
import com.sun.codemodel.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rramirezb on 18/12/2014.
 */
public class DaoGenerator {

    public static final String DAO_SUFFIX = "Dao";

    public static void generate(ProcessingEnvironment processingEnv, Element element, ModelData modelData, FileGenerator fileGenerator) {
        if (element instanceof TypeElement && element.getKind().isClass()) {
            generate((TypeElement) element, modelData, fileGenerator);
        }
    }


    public static void generate(TypeElement element, ModelData modelData, FileGenerator fileGenerator) {

        try {
            if (!modelData.getIdTypeClass().equals("")) {
                String daoClassName = Tool.generateClassName(modelData, DAO_SUFFIX);

                JCodeModel codeModel = fileGenerator.getCodeModel(daoClassName);
                AtomicBoolean alreadyDefined = new AtomicBoolean();
                JDefinedClass daoClass = Tool.getPublicFinalClass(fileGenerator, daoClassName, false, alreadyDefined);
               generate(element,daoClass,modelData,codeModel);

            }
        } catch (JClassAlreadyExistsException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void generate(TypeElement element, JDefinedClass daoClass, ModelData modelData, JCodeModel codeModel) {


        if (!modelData.getIdTypeClass().equals("")) {


            AtomicBoolean alreadyDefined = new AtomicBoolean();
            String whereClassName = modelData.getWhereClass();
            daoClass.
                    _extends(
                            Tool.getClassWithWildcardsString(
                                    codeModel,
                                    XDao.class, modelData.getModelClass(), modelData.getIdTypeClass(), whereClassName));
            addConstructor(daoClass, element, whereClassName, codeModel);
            addGetMethodByInstance(daoClass, modelData, codeModel);
            if (Tool.isJavaPlatform(modelData.getPlatform()) ||
                    Tool.isBothPlatforms(modelData.getPlatform())
                    ) {
                addGetMethodByDBInfo(daoClass, modelData, codeModel);
            }
            if (Tool.isAndroidPlatform(modelData.getPlatform()) ||
                    Tool.isBothPlatforms(modelData.getPlatform())
                    ) {
                addGetMethodByContextAndDBInfo(daoClass, modelData, codeModel);
            }
            //DBInstance instance = new MyOrmHelper(new MyDBInfo()).getInstance();

        }

    }

    protected static void addConstructor(JDefinedClass daoClass, TypeElement element, String whereClassName, JCodeModel codeModel) {

        //            daoClass.addConstructor [
//                    addParameter("db", DBInstance.newTypeReference)
//            addParameter("modelClass", Class.newTypeReference(annotatedClass.newTypeReference))
//            addParameter("whereClass", Class.newTypeReference(whereClass.newTypeReference))
//            body = '''super(db, modelClass, whereClass);'''
//            ]
        JMethod constructor = daoClass.constructor(JMod.PUBLIC);

        constructor.param(codeModel.ref(DBInstance.class), "db");
        constructor.param(Tool.getClassWithWildcardsString(codeModel, Class.class,
                element.getQualifiedName().toString()), "modelClass");
        constructor.param(Tool.getClassWithWildcardsString(codeModel, Class.class,
                whereClassName), "statementClass");
        constructor.body().directStatement("super(db, modelClass, statementClass);");
    }

    protected static void addGetMethodByInstance(JDefinedClass daoClass, ModelData modelData, JCodeModel codeModel) {
//        daoClass.addMethod("get") [
//        static = true
//        final = true
//        visibility = Visibility.PUBLIC
//        returnType = daoClass.newTypeReference
//        addParameter("db", DBInstance.newTypeReference)
//        body = '''«daoClass.simpleName» instance = new «daoClass.simpleName»(db,
//        «annotatedClass.simpleName».class, «whereClass.simpleName».class);
//        return instance;'''
//        ]

        JClass daoRef = codeModel.ref(daoClass.name());
        JClass modelRef = codeModel.ref(modelData.getModelClass());
        JClass whereRef = codeModel.ref(modelData.getWhereClass());
        JMethod method = daoClass.method(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, daoRef, "get");
        JVar dbParam = method.param(codeModel.ref(DBInstance.class), "db");
        JVar instance = method.body().decl(daoRef, "instance", JExpr._new(daoRef).arg(dbParam).arg(JExpr.dotclass(modelRef)).arg(JExpr.dotclass(whereRef)));
        method.body()._return(instance);
    }

    protected static void addGetMethodByDBInfo(JDefinedClass daoClass, ModelData modelData, JCodeModel codeModel) {

        //DBInstance instance = new MyOrmHelper(new MyDBInfo()).getInstance();

        JClass ormHelperRef = codeModel.ref(modelData.getOrmHelperClass());
        JClass dbInstanceRef = codeModel.ref(DBInstance.class);
        JClass daoRef = codeModel.ref(daoClass.name());
        JClass modelRef = codeModel.ref(modelData.getModelClass());
        JClass whereRef = codeModel.ref(modelData.getWhereClass());

        JMethod method = daoClass.method(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, daoRef, "get");
        JVar dbParam = method.param(codeModel.ref(JavaDatabaseInfo.class), "dbInfo");

        //new OrmHelper
        JInvocation dbInstanceCall = JExpr._new(ormHelperRef).arg(dbParam).invoke("getInstance");

        JVar dbInstance = method.body().decl(dbInstanceRef, "dbInstance", dbInstanceCall);

        JInvocation getInvoke = JExpr.invoke("get").arg(dbInstance);
        JVar dao = method.body().decl(daoRef, "dao", getInvoke);
        method.body()._return(dao);
    }

    protected static void addGetMethodByContextAndDBInfo(JDefinedClass daoClass, ModelData modelData, JCodeModel codeModel) {

        //DBInstance instance = new MyOrmHelper(new MyDBInfo()).getInstance();

        JClass ormHelperRef = codeModel.ref(modelData.getOrmHelperClass());
        JClass dbInstanceRef = codeModel.ref(DBInstance.class);
        JClass daoRef = codeModel.ref(daoClass.name());
        JClass modelRef = codeModel.ref(modelData.getModelClass());
        JClass whereRef = codeModel.ref(modelData.getWhereClass());

        JMethod method = daoClass.method(JMod.PUBLIC | JMod.STATIC | JMod.FINAL, daoRef, "get");
        JVar context = method.param(Context.class, "context");
        JVar dbParam = method.param(codeModel.ref(AndroidDatabaseInfo.class), "dbInfo");

        //new OrmHelper
        JInvocation dbInstanceCall = JExpr._new(ormHelperRef).
                arg(context).arg(dbParam).
                invoke("getInstance");

        JVar dbInstance = method.body().decl(dbInstanceRef, "dbInstance", dbInstanceCall);

        JInvocation getInvoke = JExpr.invoke("get").arg(dbInstance);
        JVar dao = method.body().decl(daoRef, "dao", getInvoke);
        method.body()._return(dao);
    }
}
