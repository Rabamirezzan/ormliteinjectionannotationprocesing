package com.ormliteinjection.annotationprocessing.injection;

/**
 * Created by rramirezb on 19/12/2014.
 */
public @interface DaoMetaData {
    String daoPackage();
}
