package com.ormliteinjection.annotationprocessing.injection;


import com.codeinjection.tools.FileGenerator;
import com.codeinjection.tools.log.Log;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.j256.ormlite.field.DatabaseField;
import com.ormliteinjection.annotationprocessing.injection.data.ModelData;
import com.ormliteinjection.core.db.helper.XDBHelper;
import com.ormliteinjection.core.db.injection.DaoInjectionInfo;
import com.ormliteinjection.core.db.xtend.DatabaseFieldTool;
import com.sun.codemodel.*;

import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rramirezb on 18/12/2014.
 */
public class Tool {

    public static String toUpperFirstLetter(String text) {
        return text.substring(0, 1).toUpperCase().concat(text.substring(1, text.length()));
    }
    public static String toLowerFirstLetter(String text) {
        return text.substring(0, 1).toLowerCase().concat(text.substring(1, text.length()));
    }

    public static ModelData addModelClassData(TypeElement element, ModelData modelData) {
        modelData = getValidModelData(modelData);
        modelData.setModelClass(element.getQualifiedName().toString());
        TypeMirror idType = getFieldIdType(element);
        if (idType != null) {
            modelData.setIdTypeClass(idType.toString());
        }
        return modelData;
    }

    public static ModelData addWhereClassData(TypeElement element, ModelData modelData) {

        modelData = getValidModelData(modelData);

        String xWhereClassName =
                Tool.generateClassName(modelData, StatementGenerator.STATEMENT_SUFFIX);


        modelData.setWhereClass(xWhereClassName);

        return modelData;
    }


    public static String getDaoName(ModelData modelData, TypeElement element, String daoSuffix) {
        return String.format("%s.%s%s", modelData.getDaoPackage(), element.getSimpleName(), daoSuffix);
    }




//    public static Writer generateClass(Writer writer) {
//        Writer writer = null;
//        try {
//            Tool.printInfo("Creando: %s", qualifiedNameClass);
//            JavaFileObject source = processingEnv.getFiler().createSourceFile(qualifiedNameClass);
//            writer = source.openWriter();
//        }
//        catch(Exception fe){
//            try {
//                FileObject source = processingEnv.getFiler().getResource(StandardLocation.SOURCE_OUTPUT, getPackageName(qualifiedNameClass),getClassName(qualifiedNameClass)+".java");
//                boolean deleted=false; //new File(source.toUri()).delete();
//                Tool.printInfo("tratando de borrar %s? ", source.toUri());
//                FileDeleteStrategy.FORCE.delete(new File(source.toUri()));
//
//                Tool.printInfo("Recreando: %s", qualifiedNameClass);
//                source = processingEnv.getFiler().createSourceFile(qualifiedNameClass);
//                writer = source.openWriter();
//            } catch (IOException e) {
//                Tool.printWarning("Nel pastel recreación %s", Tool.getStacktrace(e));
//                e.printStackTrace();
//            }
//        }
//
//        return writer;
//    }

    private static CharSequence getPackageName(String qualifiedNameClass) {
        return qualifiedNameClass.substring(0, qualifiedNameClass.lastIndexOf('.'));
    }

    public static JClass getClassWithWildcardsClass(JCodeModel codeModel, Class<?> _class, Class<?>... wildcards) {
        JClass result;
        if (wildcards.length > 0) {
            result = codeModel.ref(_class).narrow(wildcards);
        } else {
            result = codeModel.ref(_class);
        }
        return result;
    }

    public static JClass getClassWithWildcardsString(JCodeModel codeModel, Class<?> _class, String... wildcards) {

        JClass result;
        if (wildcards.length > 0) {
            JClass[] classes = new JClass[wildcards.length];
            int i = 0;
            for (String wildcard : wildcards) {
                classes[i++] = codeModel.directClass(wildcard);
            }
            result = codeModel.ref(_class).narrow(classes);
        } else {
            result = codeModel.ref(_class);
        }

        return result;
    }

    public static JClass getClassWithWildcards(JCodeModel codeModel, String qualifiedNameClass, Class<?>... wildcards) {
        return codeModel.ref(qualifiedNameClass).narrow(wildcards);
    }

    public static String getPackageName(TypeElement element, String defaultName) {
        String name = defaultName;
        Element ee = element.getEnclosingElement();
        if (ee instanceof PackageElement) {
            name = ((PackageElement) ee).getQualifiedName().toString();
        }
        return name;
    }

    public static List<VariableElement> getFields(TypeElement element) {
        List<VariableElement> fields = ElementFilter.fieldsIn(element.getEnclosedElements());


        return fields;
    }

    public static <A extends Annotation> List<VariableElement> getFieldsWithAnnotation(TypeElement element, final Class<A> annotation) {
        List<? extends VariableElement> elements = getFields(element);
        Iterable<? extends VariableElement> result = Iterables.filter(elements, new Predicate<VariableElement>() {
            @Override
            public boolean apply(VariableElement input) {
                A ann = input.getAnnotation(annotation);
                return ann != null;
            }
        });

        return Lists.newArrayList(result);
    }

    public static TypeMirror getFieldIdType(TypeElement element) {


        List<VariableElement> fields = getFields(element);


        VariableElement idField = null;
        try {
            idField = Iterables.find(fields, new Predicate<VariableElement>() {
                @Override
                public boolean apply(VariableElement field) {

                    return isId(field);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        TypeMirror type = null;
        if (idField != null) {

            type = idField.asType();
//            MessagePrinter.printInfo(DaoProcessor.PRO_ENV, null,
//                    "Field of %s, is a %s, type %s", element.getQualifiedName(), idField.getSimpleName(), type.toString());
            System.out.printf("Field of %s, is a %s, type %s", element.getQualifiedName(), idField.getSimpleName(), type.toString());
        }

        return type;
    }

    public static boolean isId(Element field) {
        DatabaseField ann = null;
        try {
            ann = field.getAnnotation(DatabaseField.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean result = false;
        if (ann != null) {
            result = DatabaseFieldTool.isId(ann);
        }
        return result;
    }

    public static boolean isForeign(VariableElement field) {
        return DatabaseFieldTool.isForeign(field);
    }

    public static TypeMirror getType(VariableElement field) {
        return field.asType();
    }

    public static String getName(VariableElement element) {
        return DatabaseFieldTool.getName(element);
    }

    public static void printError(Throwable e) {

        Log.e("INIT", "%s", getStacktrace(e));
    }


    public static String getStacktrace(Throwable e) {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        String result;
        try {
            array = new ByteArrayOutputStream();
            PrintWriter writer = new PrintWriter(array);
            e.printStackTrace(writer);
            writer.flush();
            writer.close();
            result = array.toString();
        } catch (Exception e1) {
            result = e1.getMessage();
            e1.printStackTrace();
        }
        return result;
    }

    public static void printWarning(Throwable e) {
        Log.w("INIT", "Exception %s", getStacktrace(e));
    }

    public static void printWarning(String format, Object... params) {
        Log.w("INIT", format, params);
    }

    public static void printInfo(String format, Object... params) {
        try {

            Log.i("INIT", format, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addOrmHelperInfo(Element element, ModelData modelData) {
        String daoPackage = OrmHelperGenerator.DAO_PACKAGE_DEFAULT;
        String ormHelper = OrmHelperGenerator.ORM_HELPER_CLASS_DEFAULT;
        String dbRef = OrmHelperGenerator.DATABASE_REF_CLASS_DEFAULT;
        if (element != null) {


            try {
                DaoInjectionInfo ann = element.getAnnotation(DaoInjectionInfo.class);
                Tool.printInfo("AnnotatedClass %s ann: %s", element.getSimpleName(), ann);
                if (ann != null) {
                    if (!isNullOrEmptyString(ann.daoPackage())) {
                        daoPackage = ann.daoPackage();
                    }

                    if (!isNullOrEmptyString(ann.ormHelperName())) {
                        ormHelper = ann.ormHelperName();
                    }
                    if (!isNullOrEmptyString(ann.daosRefName())) {
                        dbRef = ann.daosRefName();
                    }

                    modelData.setPlatform(ann.platform());

                }
            } catch (Exception e1) {
                Tool.printWarning(e1);
                e1.printStackTrace();
            }

        }

        modelData.setDaoPackage(daoPackage);
        modelData.setDbClass(String.format("%s.%s", daoPackage, dbRef));
        modelData.setOrmHelperClass(String.format("%s.%s", daoPackage, ormHelper));

    }

    public static void addOrmHelperInfo(Set<? extends Element> elements, ModelData modelData) {
        Element e = null;
        if (!elements.isEmpty()) {
            Iterator<? extends Element> it = elements.iterator();
            if (it.hasNext()) {
                e = it.next();


            }
        }

        addOrmHelperInfo(e, modelData);

    }

    private static boolean isNullOrEmptyString(String value) {

        boolean result = value == null || value.equals("");
        return result;
    }

    private static boolean isValidDaoPackage(ModelData modelData) {
        modelData = getValidModelData(modelData);
        return !isNullOrEmptyString(modelData.getDaoPackage());

    }

    private static boolean isValidOrmHelper(ModelData modelData) {
        modelData = getValidModelData(modelData);
        boolean result = !isNullOrEmptyString(modelData.getOrmHelperClass());
        return result;
    }

    public static ModelData getValidModelData(ModelData modelData) {
        ModelData result = modelData;
        if (modelData == null) {
            result = new ModelData();
        }
        return result;
    }

    public static String getClassName(String qualifiedName) {
        String result = "";
        if (!isNullOrEmptyString(qualifiedName)) {
            int index = qualifiedName.lastIndexOf('.');
            result = qualifiedName.substring(index + 1, qualifiedName.length());
        }

        System.out.printf("The qualifiedName is %s, name is %s\n", qualifiedName, result);
        //Tool.printInfo("The qualifiedName is %s, name is %s", qualifiedName, result);
        return result;
    }

    public static String generateClassName(ModelData modelData, String suffix) {
        JCodeModel codeModel = new JCodeModel();
        String className = Tool.getClassName(modelData.getModelClass());

        //pacakge.name.suffix
        return String.format("%s.%s%s", modelData.getDaoPackage(), className, suffix);
    }

    public static ModelData addClassesToStoreInfo(ModelData modelData, String defaultName) {
        modelData = getValidModelData(modelData);
        modelData.setClassesToStore(String.format("%s.%s", modelData.getDaoPackage(), defaultName));
        return modelData;
    }

    public static JDefinedClass getPublicFinalClass(FileGenerator fileGen, String clsName, boolean recreate, AtomicBoolean alreadyDefined) throws JClassAlreadyExistsException {
        return fileGen.getJDefinedClass(JMod.PUBLIC | JMod.FINAL, clsName, ClassType.CLASS, recreate, alreadyDefined);
    }

    public static void setPlatform(ModelData modelData, Set<? extends Element> elements) {

    }

    public static FileObject getResource(ProcessingEnvironment processingEnv, String resource) {

        FileObject file = null;


        Filer filer = processingEnv.getFiler();
        try {
            file
                    = filer.getResource(StandardLocation.SOURCE_OUTPUT, "", resource);
        } catch (IOException e) {
            Tool.printWarning(e);
            e.printStackTrace();

        }

        return file;
    }

    public static FileObject createResource(ProcessingEnvironment processingEnv, String resource) {

        FileObject file = null;


        Filer filer = processingEnv.getFiler();

        try {
            file = filer.createResource(StandardLocation.SOURCE_OUTPUT, "", resource);


        } catch (IOException e1) {
            Tool.printWarning(e1);
            e1.printStackTrace();
        }


        return file;
    }

    private static final String ormliteconfigfile = "OrmliteInjection.config";

    public static void append(FileObject file, String text) {
        if (file != null) {
            Writer writer = null;
            try {
                writer
                        = file.openWriter();
                writer.append(text);
                writer.flush();

            } catch (IOException e) {
                Tool.printWarning(e);
                e.printStackTrace();
            } finally {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    public static void saveModelData(ModelData modelData, ProcessingEnvironment processingEnv) {
//        String ormliteInjectionConfigFile = "ormliteinjection.config";
//        try {
//            String modelDataJson = JSON.std.asString(modelData);
//            FileObject file = createResource(processingEnv, ormliteInjectionConfigFile);
//            append(file, modelDataJson + "\n");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public static ModelData readModelData(ProcessingEnvironment processingEnv) {
//        String ormliteInjectionConfigFile = "ormliteinjection.config";
//        FileObject file = getResource(processingEnv, ormliteInjectionConfigFile);
//        Tool.printInfo("existe %s ? %s", ormliteInjectionConfigFile, file != null);
//
//        ModelData result = null;
//        if (file != null)
//            try {
//
//                BufferedReader reader = new BufferedReader(new InputStreamReader(file.openInputStream()));
//
//                String json = null;
//                try {
//                    json = reader.readLine();
//                    Tool.printInfo("el file %s contiene %s", ormliteInjectionConfigFile, json);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                reader.close();
//                result = JSON.std.beanFrom(ModelData.class, json);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        return result;
//    }

    public static boolean isJavaPlatform(XDBHelper.Platform platform) {
        return XDBHelper.Platform.JAVA.equals(platform) ;
    }

    public static boolean isAndroidPlatform(XDBHelper.Platform platform) {
        return XDBHelper.Platform.ANDROID.equals(platform) ;
    }

    public static boolean isBothPlatforms(XDBHelper.Platform platform) {
        return XDBHelper.Platform.BOTH.equals(platform);
    }



}
