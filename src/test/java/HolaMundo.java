import com.j256.ormlite.field.DatabaseField;
import com.ormliteinjection.core.db.injection.DaoInjection;

/**
 * Created by rramirezb on 09/02/2015.
 */
@DaoInjection
public class HolaMundo {
    @DatabaseField(generatedId = true)
    Integer id;
    @DatabaseField
    String ok;
}
