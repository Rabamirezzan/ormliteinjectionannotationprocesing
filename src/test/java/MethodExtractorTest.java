import com.codeinjection.tools.FileGenerator;
import com.google.testing.compile.CompilationRule;
import com.ormliteinjection.annotationprocessing.injection.StatementGenerator;
import com.ormliteinjection.annotationprocessing.injection.Tool;
import com.ormliteinjection.annotationprocessing.injection.data.ModelData;
import com.ormliteinjection.core.db.helper.XDBHelper;
import com.sun.codemodel.JCodeModel;
import org.junit.Before;
import org.junit.Rule;

import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

/**
 * Created by rramirezb on 22/01/2015.
 */
public class MethodExtractorTest {
    public @Rule
    CompilationRule rule = new CompilationRule();
    private Elements elements;
    private Types types;


    @Before
    public void setup() {
        elements = rule.getElements();
        types = rule.getTypes();
    }


//    @Test
    public void test(){

        try {
            TypeElement typeElement = elements.getTypeElement(HolaMundo.class.getName());


            ModelData modelData = new ModelData();
            modelData.setClassesToStore("pack.clasesToStore");
            modelData.setDaoPackage("ok.ok.ok");
            modelData.setDbClass("db.DbClass");
            modelData.setIdTypeClass("Integer");
            modelData.setModelClass(HolaMundo.class.getName());
            modelData.setWhereClass("okok.HolamUndoWhere");
            modelData.setPlatform(XDBHelper.Platform.JAVA);
            modelData.setOrmHelperClass("OkWorksNow");
            FileGenerator fileGenerator = new FileGenerator();
            //DaoGenerator.generate(typeElement, modelData, fileGenerator);

            StatementGenerator.generate(typeElement, modelData, fileGenerator);
            String daoClassName = Tool.generateClassName(modelData, "Dao");
            JCodeModel codeModel = fileGenerator.getCodeModel(daoClassName);

           // List<JCodeModel> codes = fileGenerator.getCodeModelCol();

//            for (int i = 0; i < codes.size(); i++) {
//                JCodeModel jCodeModel =  codes.get(i);
//                String code = FileGenerator.getCode(jCodeModel);
//                System.out.printf("%s\n", code);
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Test
    public void test02(){

    }
}
