
import com.codeinjection.tools.FileGenerator;
import com.sun.codemodel.JClassAlreadyExistsException;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import org.junit.Assert;
import org.junit.Test;

import java.io.Serializable;

/**
 * Created by rramirezb on 29/12/2014.
 */
public class _classAgain {
    @Test
    public void test(){
        JCodeModel codeModel = new JCodeModel();

        boolean noExc = false;
        try {
            JDefinedClass cls = codeModel._class("com.hola.Mundo");
            cls._implements(Serializable.class);


            cls = codeModel._getClass("com.hola.Mundo");
            cls._implements(Runnable.class);

            String code = FileGenerator.getCode(codeModel);
            System.out.println(code);
            noExc = true;
        } catch (JClassAlreadyExistsException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("No exception", noExc);
    }
}
