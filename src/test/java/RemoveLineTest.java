import org.junit.Assert;
import org.junit.Test;import java.lang.String;

/**
 * Created by rramirezb on 18/12/2014.
 */
public class RemoveLineTest {
    @Test
    public void test(){
        String r = "     --------THIS COMMENT WILL BE DELETED------------   \nNO_DELETED".replaceAll("(^[^\n]*-+[^\n]*-+[^\n]*\n)","");
        Assert.assertEquals("Deleting CodeModel line", "NO_DELETED",r);
        String c;
    }
}
